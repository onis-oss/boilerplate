package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
)

var rootCmd = &cobra.Command{
	Use:   "boilerplate",
	Short: "Boilerplate command",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Hello Boilerplate!")
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
