FROM ubuntu:20.04

WORKDIR /work

# install OS packages
COPY install-build.sh /work
RUN ./install-build.sh

COPY bin/ /work/bin/

# install hermit
ENV PATH="/work/bin:${PATH}"
RUN set -eux;\
  git init;\
  hermit init

# install other dependencies
COPY Makefile package.json yarn.lock /work/

RUN set -eux;\
  eval "$(hermit env --activate)";\
  make install
