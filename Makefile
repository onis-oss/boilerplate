MODULE=gitlab.com/onis-oss/tlc/tlc/sanity/builder

MEGA_LINTER_VERSION=v4.46.0

.PHONY: all
all:
	make install
	make build
	make test

.PHONY: all-docker
all-docker:
	docker run -it -v ${PWD}:/work ${MODULE}/builder make all

.PHONY: build
build:
	go build .
	yarn workspace webapp build

.PHONY: builder
builder:
	docker build -f builder.Dockerfile -t ${MODULE} .

.PHONY: clean
clean:
	go clean -cache -modcache -i -r

.PHONY: dev
dev:
	yarn workspace webapp dev

.PHONY: format
format:
	gofmt -l -w .

.PHONY: install
install:
	hermit install
	. ./bin/activate-hermit
	hermit shell-hooks --bash
	npm install --global yarn
	yarn install
	touch node_modules/go.mod

.PHONY: lint
lint:
	docker run -it -v ${PWD}:/tmp/lint nvuillam/mega-linter:${MEGA_LINTER_VERSION}

.PHONY: run
run: build
	./boilerplate

.PHONY: test
test:
	go test -tags test -v -coverpkg=./... -coverprofile=profile.cov ./...
	go tool cover -html=profile.cov -o profile.html
	go tool cover -func profile.cov
	go vet ./...
  # validate formatting (use make format to fix)
	[ "`gofmt -l -s .`" = "" ]
  # open coverage
	go tool cover -html=profile.cov

.PHONY: test-docker
test-docker:
	docker run -it -v ${PWD}:/work ${MODULE} make
