//go:build !test

package main

import (
	"gitlab.com/onis-oss/public/boilerplate/cmd"
)

func main() {
	cmd.Execute()
}
